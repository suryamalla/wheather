import logo from "./logo.svg";
import "./App.css";
import { useState, useEffect } from "react";
import axios from "axios";


function App() {
  let sn=1;
  const searchValue = (e) => {
    console.log("e", e.target.value);
    setSearch(e.target.value);
  };

  const [data, setData] = useState([]);
  const [search, setSearch] = useState("");
  // const [datapost, setDatapost] = useState({
  //   city: ""
  // });

  /* Filter Function */
  function myFunction(keyword) {
    const filterKeyword = keyword.toUpperCase();
    const filteredWords = data.filter(
      (res) => res?.city?.toUpperCase().indexOf(`${filterKeyword}`) > -1
    );
    return filteredWords;
  }

  // function onchange(e){
  //   setDatapost({
  //     ...datapost,
  //     [e.target.name]: e.target.value
  //   })
  //   }

  useEffect(() => {
    if (search) {
      const result = myFunction(search);
      setData(result);
    }
  }, [search]);

  useEffect(() => {
    axios.get(`http://localhost:4444/data?q=${search}`).then((res) => {
      setData(res.data);
      // console.log(res.data);
    });
    // fetchApi();
  }, []);

  return (
    <>
      {data.map((item) => {
        if(sn==1){
          sn=sn+2;
        return (
          <div
            className="main"
            key={item.id}
            style={{ backgroundImage: `url(${item.image})`,backgroundRepeat:"no-repeat", backgroundSize:"cover"
            // background-size: cover;
             }}
          >
            <div className="heading">
              <img id="logo" src={logo} className="App-logo" alt="logo" />
              <span id="heading"> Find Todays Weather Here</span>
            </div>
            <div className="container">
              <div className="cont1">
                <div className="cont11">
                  <span className="city">
                    {item.city},{item.country}
                  </span>{" "}
                  <br /> <span>{item.date}</span>
                </div>
                {/* {console.log(city1)}*/}
                <div className="cont12">
                  <form action="" onSubmit={searchValue}>
                    {/* <div className="cont121">
                <span>Country Name</span>
                <br />
                <span>
                   <input type="text" id="country"  
                    onChange={searchValue}

                    />
                </span>
              </div> */}
                    <div className="cont122">
                      <span>City Name</span>
                      <br />
                      <span>
                        <input
                          type="text"
                          id="city"
                          name="city"
                          onChange={searchValue}
                        />
                      </span>
                      <br />
                      {/* <button type="submit" id="button"
                 onClick={searchValue}
                 >Get Weather</button> */}
                    </div>
                  </form>
                </div>
              </div>
              <div className="cont2">
                <div className="cont21">{item.session}</div>
                <br />
                <div className="cont22">{item.currenttemp}'c</div>
              </div>

              <div className="cont3">
                <div className="cards">
                  <div className="subcard">Minimum Temperature</div>
                  <br />
                  <div className="subcard">{item.minimumtemp}'c</div>
                </div>
                <div className="cards">
                  <div className="subcard">Maximum Temperature</div>
                  <br />
                  <div className="subcard">{item.maximumtemp}'c</div>
                </div>
                <div className="cards">
                  <div className="subcard">Humidity</div>
                  <br />
                  <br />
                  <div className="subcard">{item.humidity}</div>
                </div>
                <div className="cards">
                  <div className="subcard">Latitude</div>
                  <br />
                  <br />
                  <div className="subcard">{item.latitude}</div>
                </div>
                <div className="cards">
                  <div className="subcard">Longitude</div>
                  <br />
                  <br />
                  <div className="subcard">{item.longitude}</div>
                </div>
                <div className="cards">
                  <div className="subcard">Sunrise</div>
                  <br />
                  <br />
                  <div className="subcard">{item.sunrise} am</div>
                </div>
                <div className="cards">
                  <div className="subcard">Sunset</div>
                  <br />
                  <br />
                  <div className="subcard">{item.sunset} pm</div>
                </div>
              </div>
            </div>
          </div>
        );
      }
      })}
    </>
  );
}

export default App;
